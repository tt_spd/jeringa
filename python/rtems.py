# Copyright (C) 2014-2015 Martin Galvan.
# This file is part of Jeringa.
#
# Jeringa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeringa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeringa.  If not, see <http://www.gnu.org/licenses/>.

import gdb

class RTEMSStackManager(object):
    """A class which encapsulates RTEMS-specific access to stack structures.

    This class implements the StackManager interface.
    """

    def __init__(self, threadIndex=1):
        """Constructor.

        Arguments:
            threadIndex: An integer representing the number of the thread
            whose stack we'll delimit. Right now we only want thread number 1.
        """

        self.stackString = ("((struct Thread_Control_struct *)"
            "_POSIX_Threads_Information.local_table[%d])->Start.Initial_stack" % threadIndex)
        self.stackAddress = self.stackString + ".area"
        self.stackSize = self.stackString + ".size"

    def getStackBeginning(self):
        """Get the beginning address of the stack.

        This method is part of the StackManager interface.

        Returns:
            An integer representing the beginning address of the stack.
        """

        stackBeginning = gdb.parse_and_eval(self.stackAddress + " + " + self.stackSize)

        return long(stackBeginning)
