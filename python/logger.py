# Copyright (C) 2014-2015 Martin Galvan.
# This file is part of Jeringa.
#
# Jeringa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeringa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeringa.  If not, see <http://www.gnu.org/licenses/>.

import gdb

class Logger(object):
    """A class used to log messages."""

    _muted = False

    @staticmethod
    def setMuted(muted):
        """Set the logger's muted status.

        Arguments:
            muted: A boolean indicating whether to mute the logger's output.
        """

        Logger._muted = muted

        if muted:
            gdb.execute("set logging on")
        else:
            gdb.execute("set logging off")

    @staticmethod
    def log(message):
        """Log a message.

        Right now this method simply prints the message to sys.stdout.

        Arguments:
            message: A string to log.
        """

        if not Logger._muted:
            print(message)
