# Copyright (C) 2014-2015 Martin Galvan.
# This file is part of Jeringa.
#
# Jeringa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeringa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeringa.  If not, see <http://www.gnu.org/licenses/>.

from logger import Logger
import re
import gdb

class RobustoDetector(object):
    """A class which identifies and executes RobustO functions.

    This class implements the SCCDetector interface.
    """

    def __init__(self, robustoProgramExitFunctions=["robustoVotingErr"]):
        """Constructor.

        Arguments:
            robustoProgramExitFunctions: A list of strings with the names of
            any RobustO functions which call program-terminating functions such
            as exit().
        """

        self.regex = re.compile("NSRobusto.*")
        self.SCCExecutionError = None
        self.finishedExecutingSCC = None
        self.robustoProgramExitFunctions = robustoProgramExitFunctions

    def isSCC(self, currentPC, functionName):
        """Return whether we're inside RobustO code.

        This method is part of the SCCDetector interface.

        Arguments:
            currentPC: Unused.
            functionName: A string with the name of the current function.
        Returns:
            A boolean indicating whether the current function is part of RobustO.
        """

        return self.regex.match(functionName)

    def executeSCC(self, currentPC, functionName):
        """Execute the RobustO code.

        This method is part of the SCCDetector interface.

        Arguments:
            currentPC: Unused.
            functionName: Unused.
        """

        gdb.execute("finish")

    def executeSCCAndCheck(self, currentPC, functionName):
        """Execute the RobustO code and check if any errors occurred.

        This method is part of the SCCDetector interface.

        Arguments:
            currentPC: Unused.
            functionName: Unused.
        """

        self.SCCExecutionError = False
        self.finishedExecutingSCC = False

        # Set our control breakpoints.
        self._setProgramExitDetectors()
        robustoFinishBreakpoint = RobustoFinishBreakpoint(self)
        robustoStopBreakpoint = RobustoStopBreakpoint(self)

        # Continue until we exit the current frame.
        while not self.finishedExecutingSCC and not self.SCCExecutionError:
            gdb.execute("step")

        # If any of our breakpoint still exist (i.e. we didn't hit them), delete them.
        self._clearProgramExitDetectors()

        if robustoFinishBreakpoint.is_valid():
            robustoFinishBreakpoint.delete()

        if robustoStopBreakpoint.is_valid():
            robustoStopBreakpoint.delete()

        return self.SCCExecutionError

    def setSCCExecutionError(self, SCCExecutionError):
        """Tell RobustoDetector whether an error occurred while executing the SCC.

        Arguments:
            SCCExecutionError: A boolean indicating whether an error occurred.
        """

        self.SCCExecutionError = SCCExecutionError

    def setFinishedExecutingSCC(self, finishedExecutingSCC):
        """Tell RobustoDetector that it finished executing the SCC.

        Arguments:
            finishedExecutingSCC: A boolean indicating whether we finished executing
            the SCC.
        """

        self.finishedExecutingSCC = finishedExecutingSCC

    def _setProgramExitDetectors(self):
        """Insert a RobustoProgramExitBreakpoint at each of the robustoProgramExitFunctions."""

        for programExitFunction in self.robustoProgramExitFunctions:
            RobustoProgramExitBreakpoint(programExitFunction, self)

    def _clearProgramExitDetectors(self):
        """Clear all the breakpoints at each of the robustoProgramExitFunctions."""

        for programExitFunction in self.robustoProgramExitFunctions:
            gdb.execute("clear %s" % programExitFunction)

################################################################################

class RobustoFinishBreakpoint(gdb.FinishBreakpoint):
    """A gdb.FinishBreakpoint used to check for errors while executing RobustO code.

    This breakpoint is set by RobustoDetector at the return point of the
    RobustO functions. If it detects that the call stack was corrupted and we'll
    never reach the return point, it'll let RobustoDetector know that an error
    occurred.
    """

    def __init__(self, robustoDetector):
        """Constructor.

        Arguments:
            robustoDetector: The RobustoDetector associated to this breakpoint.
        """

        super(RobustoFinishBreakpoint, self).__init__()
        self.robustoDetector = robustoDetector

    def out_of_scope(self):
        """Callback invoked by gdb when it detects that we'll never reach the address
        this breakpoint was inserted into.
        """

        self.robustoDetector.setSCCExecutionError(True)
        Logger.log("Breakpoint %d: ERROR INSIDE ROBUSTO (out_of_scope)!!" % self.number)

class RobustoStopBreakpoint(gdb.Breakpoint):
    """A breakpoint used to detect whether we're done executing RobustO code.

    When RobustoDetector steps past the RobustO function's end point,
    this will make it stop advancing so we can continue driving the target normally.
    Notice we could've achieved the same effect by overriding RobustoFinishBreakpoint's
    stop method. However, we're forced to use a separate type of breakpoint because
    of a bug in gdb itself.
    """

    def __init__(self, robustoDetector):
        """Constructor.

        Arguments:
            robustoDetector: The RobustoDetector associated to this breakpoint.
        """

        super(RobustoStopBreakpoint, self).__init__("*%d" % gdb.selected_frame().older().pc())
        self.robustoDetector = robustoDetector

    def stop(self):
        """Callback invoked when this breakpoint is reached.

        Returns:
            A boolean indicating whether we should stop at this breakpoint.
        """

        self.robustoDetector.setFinishedExecutingSCC(True)
        return True  # Stop at this breakpoint.

################################################################################

class RobustoProgramExitBreakpoint(gdb.Breakpoint):
    """A breakpoint set at any RobustO functions which call program-terminating functions
    such as exit().
    """

    def __init__(self, spec, robustoDetector):
        """Constructor.

        Arguments:
            spec: A string with the breakpoint's location.
            robustoDetector: The RobustoDetector associated to this breakpoint.
        """

        super(RobustoProgramExitBreakpoint, self).__init__(spec)
        self.robustoDetector = robustoDetector

    def stop(self):
        """Callback invoked when this breakpoint is reached.

        Returns:
            A boolean indicating whether we should stop at this breakpoint.
        """

        self.robustoDetector.setSCCExecutionError(True)
        Logger.log("Breakpoint %d: EXIT INSIDE ROBUSTO!!" % self.number)

        return True  # Stop at this breakpoint.
