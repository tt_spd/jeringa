# Copyright (C) 2014-2015 Martin Galvan.
# This file is part of Jeringa.
#
# Jeringa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeringa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeringa.  If not, see <http://www.gnu.org/licenses/>.

import gdb

class QemuConnectionManager(object):
    """A class which encapsulates QEMU-specific connection commands.

    This class implements the ConnectionManager interface.
    """

    def __init__(self, port):
        """Constructor.

        Arguments:
            port: An integer representing the port we'll connect through.
        """

        self.port = port

    def connect(self):
        """Connect to QEMU."""

        gdb.execute("target remote localhost:%s" % self.port)

    def disconnect(self):
        """Disconnect from QEMU."""

        gdb.execute("disconnect")

    def resetTarget(self):
        """Reset QEMU."""

        gdb.execute("monitor system_reset")
