# Copyright (C) 2014-2015 Martin Galvan.
# This file is part of Jeringa.
#
# Jeringa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeringa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeringa.  If not, see <http://www.gnu.org/licenses/>.

from logger import Logger
import itertools
import random
import gdb

class SymbolInjector(object):
    """Performs an injection using the symbol table to get info on a local
    variable or argument.

    Uses a PhysicalInjector to perform the actual injection.
    Can only inject safely if there are valid local variable symbols
    at the injection point (i.e. it's not a function prologue or epilogue).
    This class implements the Injector interface.
    """

    def __init__(self):
        """Constructor."""

        self.physicalInjector = PhysicalInjector()

    def inject(self):
        """Perform the injection.

        This method is part of the Injector interface.
        """

        localSymbols = self._getLocalSymbols(gdb.selected_frame().block())

        if localSymbols:
            randomIndex = random.randint(0, len(localSymbols) - 1)
            Logger.log("Attacking: %s" % localSymbols[randomIndex].name)

            #gdb.lookup_symbol("value")[0].value(gdb.newest_frame())
            localValue = localSymbols[randomIndex].value(gdb.newest_frame())

            return self._injectInVariable(localValue)

    def canInjectAt(self, injectionPC):
        """Return whether we can perform an injection at the given PC.

        We can only inject if there are valid local symbols available.
        This method is part of the Injector interface.

        Arguments:
            injectionPC: An integer representing the target PC.
        Returns:
            A boolean indicating whether there are valid local symbols available
            at injectionPC.
        """

        try:
            result = (not gdb.stack_may_be_invalid(injectionPC) and
                      self._getLocalSymbols(gdb.block_for_pc(injectionPC)))
        except:
            # gdb.stack_may_be_invalid may throw an exception if injectionPC points
            # to hand-written assembly.
            result = False

        return result

    def _getLocalSymbols(self, block):
        """Return a list of variable/argument symbols visible at the local scope,
        up to the current static block.

        Arguments:
            block: A gdb.block we'll representing the scope in which the returned
            symbols are visible.
        Returns:
            A list of the gdb.symbols visible at the given scope.
        """

        symbols = []
        predicate = lambda symbol: symbol.is_variable or symbol.is_argument
        filteredBlock = itertools.ifilter(predicate, block)

        if block.superblock:
            for symbol in filteredBlock:
                symbols.append(symbol)

            if not block.superblock.is_static:
                symbols += self._getLocalSymbols(block.superblock)

        return symbols

    def _injectInVariable(self, variable):
        """Flip a bit at a random offset inside a given variable.

        Arguments:
            variable: A gdb.Value representing the variable we'll inject into.
        Returns:
            A boolean indicating whether or not the injection was successful.
        """

        variableAddress = variable.address
        variableSize = variable.type.sizeof

        if variableAddress:  # If the variable is addressable (i.e. not a register)..
            offset = random.randint(0, variableSize - 1)
            self.physicalInjector.injectAt(long(variableAddress) + offset)
            success = True
        else:
            Logger.log("Target not addressable")
            success = False

        return success

################################################################################

class StackInjector(object):
    """Performs an injection somewhere at a thread's stack space.

    Uses a PhysicalInjector to perform the actual injection.
    This class implements the Injector interface.
    """

    def __init__(self, stackManager):
        """Constructor.

        Arguments:
            stackManager: an OS-dependant stackManager used to get info
            on the stack.
        """

        self.stackManager = stackManager
        self.physicalInjector = PhysicalInjector()

    def inject(self):
        """Perform the injection.

        This method is part of the Injector interface.

        Returns:
            Always True, since we can successfully inject at any PC.
        """

        stackBeginning = self.stackManager.getStackBeginning()
        stackEnd = long(gdb.parse_and_eval("$sp"))

        injectionAddress = random.randint(stackEnd, stackBeginning)

        self.physicalInjector.injectAt(injectionAddress)

        return True  # These injections will always succeed.

    def canInjectAt(self, injectionPC):
        """Return whether we can perform an injection at the given PC.

        This method is part of the Injector interface.

        Arguments:
            injectionPC: An integer representing the target PC.
        Returns:
            Always True, since we can successfully inject at any PC.
        """

        return True

################################################################################

class PhysicalInjector(object):
    """Performs a bit-flip at a random bit inside the 8-bit byte at the given address."""

    def injectAt(self, address):
        """Perform the actual bit-flip.

        Arguments:
            address: An integer representing the address of the byte we'll inject into.
        """

        randomBit = random.randint(0, 7)  # Assuming 8-bit byte addressing

        Logger.log("Attacking: %#x; bit %d" % (address, randomBit))
        gdb.execute("set *%d ^= (1 << %d)" % (address, randomBit))
