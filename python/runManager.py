# Copyright (C) 2014-2015 Martin Galvan.
# This file is part of Jeringa.
#
# Jeringa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeringa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeringa.  If not, see <http://www.gnu.org/licenses/>.

from targetDriver import GoldenRunDriver, InjectionRunDriver
from datetime import datetime
from logger import Logger
import random
import itertools
import gdb

class GoldenRunManager(object):
    """A class used to perform a Golden Run.

    A Golden Run is a regular target execution without fault injection. It produces
    a file with target information that we'll use to perform Injection Runs.
    GoldenRunManager performs the target execution using a GoldenRunDriver.
    """

    def __init__(self, connectionManager, SCCDetector):
        """Constructor.

        Arguments:
            connectionManager: The ConnectionManager object we'll use to connect
            to the target.
            SCCDetector: The SCCDetector we'll use to detect and execute the target's
            SCC (if any).
        """

        self.connectionManager = connectionManager
        self.targetDriver = GoldenRunDriver(SCCDetector)

    def startRunning(self, logFile, startPoints, endPoints):
        """Perform a Golden Run.

        Arguments:
            logFile: A string with the path to the Golden Run log file.
            startPoints: A list of strings with locations we may start logging from.
            endPoints: A list of strings with locations that, when reached, will
            make us stop logging.
        """

        # Connect to the target and reset it.
        self.connectionManager.connect()
        self.connectionManager.resetTarget()

        startTime = datetime.now()

        Logger.setMuted(True)

        self.targetDriver.advanceToStartPoint(startPoints)
        self.targetDriver.setEndBreakpoints(endPoints)

        # Run the target.
        with open(logFile, "w") as goldenRunLog:
            steps = self.targetDriver.proceedAndLog(goldenRunLog)

        Logger.setMuted(False)

        endTime = datetime.now()
        deltaTime = endTime - startTime

        Logger.log("\nFinal PC: %#x" % gdb.parse_and_eval("$pc"))
        Logger.log("Steps: %d" % steps)
        Logger.log("Time: %f seconds.\n" % deltaTime.total_seconds())

################################################################################

# A RunManager which manages injection runs.
# USES: ConnectionManager, InjectionRunDriver, Injector.

class InjectionRunManager(object):
    """A class used to perform Injection Runs.

    An Injection Run is a target execution in which faults are injected and the
    target is monitored to detect any errors that arise from the injections.
    It requires a Golden Run log file.
    InjectionRunManager uses an InjectionRunDriver and an Injector to perform
    its runs.
    """

    def __init__(self, connectionManager, SCCDetector, injector):
        """Constructor.

        Arguments:
            connectionManager: The ConnectionManager object we'll use to connect
            to the target.
            SCCDetector: The SCCDetector we'll use to detect and execute the target's
            SCC (if any).
            injector: The Injector we'll use to attack the target.
        """

        self.connectionManager = connectionManager
        self.targetDriver = InjectionRunDriver(SCCDetector)
        self.injector = injector
        self.crashes = 0
        self.silents = 0
        self.maxSteps = 0

    def startRunning(self, logFile, numberOfRuns):
        """Perform the Injection Runs.

        Arguments:
            logFile: A string with the path to the Golden Run log file.
            numberOfRuns: An integer with the number of Injection Runs we'll perform.
        """

        totalTime = 0

        with open(logFile, "r") as goldenRunLog:
            # Compute the maximum number of steps we can do.
            for i, line in enumerate(goldenRunLog):
                pass

            self.maxSteps = i

            if self.maxSteps != 0:
                startTime = datetime.now()

                # Get the first logged PC.
                goldenRunLog.seek(0)
                firstPC = goldenRunLog.readline().rstrip()

                # Perform the runs.
                for i in xrange(numberOfRuns):
                    self._performInjectionRun(goldenRunLog, firstPC, i)

                endTime = datetime.now()
                totalTime = endTime - startTime

                Logger.log("\n==========================================")
                Logger.log("\nCrashes: %d" % self.crashes)
                Logger.log("Total time: %f" % totalTime.total_seconds())

            else:  # File is empty.
                Logger.log("The Golden Run log is empty.")

    def _performInjectionRun(self, goldenRunLog, firstPC, iteration):
        """Perform a single Injection Run.

        Arguments:
            goldenRunLog: A file corresponding to the Golden Run log.
            firstPC: A string representing the first PC we can inject from.
            iteration: The number of this Injection Run.
        """

        crashDetected = False
        canPerformInjection = False

        # Go to the start of the file.
        goldenRunLog.seek(0)

        # Connect to the target, reset it, clear any remaining breakpoints,
        # and advance up to the first logged PC.
        self.connectionManager.connect()
        self.connectionManager.resetTarget()
        gdb.execute("delete")
        self.targetDriver.advanceTo("*%s" % firstPC)

        # Compute a random injection step and check if our injector
        # can actually inject at said step.
        while not canPerformInjection:
            goldenRunLog.seek(0)

            injectionStep = random.randint(0, self.maxSteps)
            targetPC = itertools.islice(goldenRunLog, injectionStep, None).next().rstrip()
            canPerformInjection = self.injector.canInjectAt(long(targetPC, 0))

        Logger.log("\nIteration: %d; stepis: %d" % (iteration, injectionStep))
        Logger.log("Target PC: %s\n" % targetPC)

        # Perform the injection.
        startTime = datetime.now()

        Logger.setMuted(True)

        self.targetDriver.goToInjectionPoint(goldenRunLog, injectionStep, targetPC)
        injectionSuccessful = self.injector.inject()

        if injectionSuccessful:
            crashDetected = self.targetDriver.monitorAfterInjection(goldenRunLog, injectionStep)

        Logger.setMuted(False)

        # Disconnect from the target.
        self.connectionManager.disconnect()

        # If we detected a crash, register it.
        if crashDetected:
            self.crashes += 1

        endTime = datetime.now()
        deltaTime = endTime - startTime

        print("\nTime: %f seconds." % deltaTime.total_seconds())
        print("Crashes so far: %d" % self.crashes)
