# Copyright (C) 2014-2015 Martin Galvan.
# This file is part of Jeringa.
#
# Jeringa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeringa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeringa.  If not, see <http://www.gnu.org/licenses/>.

from endBreakpoint import EndBreakpoint
from logger import Logger
import itertools
import gdb

class GoldenRunDriver(object):
    """A class used to drive the target during a Golden Run.

    It logs the addresses of all the executed instructions until it hits an EndBreakpoint.
    """

    def __init__(self, SCCDetector):
        """Constructor.

        Arguments:
            SCCDetector: The SCCDetector we'll use to detect and execute the target's
            SCC (if any).
        """

        self.SCCDetector = SCCDetector
        self.inFaultZone = False

    def proceedAndLog(self, goldenRunLog):
        """Execute the target while logging the values of its program counter.

        Arguments:
            goldenRunLog: A file corresponding to the Golden Run log.
        Returns:
            The number of executed instructions.
        """

        steps = 0
        self.setInFaultZone(True)

        # Step until we reach an EndBreakpoint.
        while self.inFaultZone:
            selectedFrame = gdb.selected_frame()
            currentPC = selectedFrame.pc()
            functionName = selectedFrame.name()

            if self.SCCDetector and self.SCCDetector.isSCC(currentPC, functionName):
                self.SCCDetector.executeSCC(currentPC, functionName)

            goldenRunLog.write("%#x\n" % gdb.selected_frame().pc())

            gdb.execute("stepi")
            steps += 1

        # Clear any remaining breakpoints.
        gdb.execute("delete")

        return steps

    def advanceToStartPoint(self, startPoints):
        """Execute the target up to the location we'll start logging from.

        Arguments:
            startPoints: A list of strings with locations we may start logging from.
        """

        # Set breakpoints at the target addresses given in the startPoints list,
        # then proceed until we hit one of said breakpoints.
        for startPoint in startPoints:
            gdb.Breakpoint(startPoint)

        gdb.execute("continue")
        gdb.execute("delete")

    def setEndBreakpoints(self, endPoints):
        """Set breakpoints at all the locations that, when reached, will
        make us stop logging.

        Arguments:
            endPoints: A list of strings with locations that, when reached, will
            make us stop logging.
        """

        for endPoint in endPoints:
            EndBreakpoint(endPoint, self)

    def setInFaultZone(self, inFaultZone):
        """Tell GoldenRunDriver whether it should keep logging.

        Arguments:
            inFaultZone: A boolean indicating whether we're still in the 'fault zone',
            that is, the range of code addresses into which we have to keep logging.
        """

        self.inFaultZone = inFaultZone

################################################################################

# A TargetDriver used for performing Injection Runs.
# USED BY: InjectionRunManager.
# USES: RobustoDetector.

class InjectionRunDriver(object):
    """A class used to drive the target during an Injection Run."""

    def __init__(self, SCCDetector):
        """Constructor.

        Arguments:
            SCCDetector: The SCCDetector we'll use to detect and execute the target's
            SCC (if any).
        """

        self.SCCDetector = SCCDetector

    def advanceTo(self, location):
        """Drive the target to the specified location.

        Arguments:
            Location: A string with the location we'll drive the target to.
        """

        gdb.execute("advance %s" % location)

    def goToInjectionPoint(self, goldenRunLog, injectionStep, targetPC):
        """Drive the target to the point where we'll perform the injection.

        Arguments:
            goldenRunLog: A file corresponding to the Golden Run log.
            injectionStep: An integer representing the line of the Golden Run log
            where we'll find the point we'll inject from.
            targetPC: An integer representing the PC we'll inject from.
        """

        ignoreCount = 0
        goldenRunLog.seek(0)

        # As we may hit the target PC before the injection point,
        # we'll ignore any appearances of the target PC in the log
        # before the injection point.
        if injectionStep > 0:
            # Make a copy of the file with every line preceding
            # the injection point line.
            slicedLog = itertools.islice(goldenRunLog, injectionStep)

            # Compute how many times we'll have to ignore the target PC
            # before reaching the injection point.
            for PCFromLog in slicedLog:
                if PCFromLog.rstrip() == targetPC:
                    ignoreCount += 1

            Logger.log("Continues: %d" % ignoreCount)

            # Advance up to the injection point by setting a breakpoint
            # at the target PC with an ignore count equal to the amount
            # of times the target PC appears before the injection point.
            targetPCBreakpoint = gdb.Breakpoint("*%s" % targetPC, temporary=True)
            targetPCBreakpoint.ignore_count = ignoreCount
            gdb.execute("continue")

    def monitorAfterInjection(self, goldenRunLog, injectionStep):
        """Execute the target until we reach the end of the Golden Run log,
        checking the PC values against the logged ones.

        Arguments:
            goldenRunLog: A file corresponding to the Golden Run log.
            injectionStep: An integer representing the line of the Golden Run log
            where we'll find the point we'll inject from.
        """

        goldenRunLog.seek(0)
        error = False
        endOfFile = False
        PCFromLog = ""
        slicedLog = itertools.islice(goldenRunLog, injectionStep, None)

        while not error and not endOfFile:
            PCFromLog = next(slicedLog, "").rstrip()

            if PCFromLog:
                error = self._proceed(PCFromLog)
            else:
                endOfFile = True

        return error

    def _proceed(self, PCFromLog):
        """Check if we're either in SCC or if an error occurred. If not, issue a 'stepi'
        command to gdb.

        Arguments:
            PCFromLog: A string with the address of the instruction we should be
            about to execute.
        Returns:
            A boolean indicating whether an error was detected.
        """

        error = False
        skippedFunctionsError = False
        currentFunctionName = gdb.selected_frame().name()

        if currentFunctionName:
            skippedFunctionsError = self._skipThroughFunctions(currentFunctionName)

        currentPC = gdb.selected_frame().pc()

        if self.rightPC(currentPC, PCFromLog) and not skippedFunctionsError:
            gdb.execute("stepi")
        else:
            error = True
            self._logError(currentPC, PCFromLog)

        return error

    def _skipThroughFunctions(self, functionName):
        """Skip through undesired functions.

        Right now this executes the SCC (if any).

        Arguments:
            functionName: A string with the name of the current function.
        Returns:
            A boolean indicating whether an error was detected.
        """

        currentPC = gdb.selected_frame().pc()
        SCCExecutionError = False

        if self.SCCDetector and self.SCCDetector.isSCC(currentPC, functionName):
            SCCExecutionError = self.SCCDetector.executeSCCAndCheck(currentPC, functionName)

        return SCCExecutionError

    def _rightPC(self, currentPC, PCFromLog):
        """Check if the current PC is the same as the logged one.

        Arguments:
            currentPC: An integer representing the address of the instruction
            we're about to execute.
            PCFromLog: A string with the address of the instruction we should be
            about to execute.
        Returns:
            A boolean indicating whether we're about to execute the right instruction.
        """

        return currentPC == long(PCFromLog, 0)

    def _logError(self, currentPC, PCFromLog):
        """Log a message indicating an error was detected.

        Arguments:
            currentPC: An integer representing the address of the instruction
            we're about to execute.
            PCFromLog: A string with the address of the instruction we should be
            about to execute.
        """

        Logger.log("ERROR")
        Logger.log("currentPC: %#x" % currentPC)
        Logger.log("PCFromLog: %#x" % long(PCFromLog, 0))
