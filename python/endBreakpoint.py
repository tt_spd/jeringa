# Copyright (C) 2014-2015 Martin Galvan.
# This file is part of Jeringa.
#
# Jeringa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeringa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeringa.  If not, see <http://www.gnu.org/licenses/>.

import gdb

class EndBreakpoint(gdb.Breakpoint):
    """A gdb.Breakpoint used to tell a GoldenRunDriver to stop logging.

    This breakpoint is set by a GoldenRunManager at the end of the 'fault zone'
    (that is, the range of PCs from which we'll be injecting faults).
    Once it's reached, GoldenRunDriver will stop logging.
    """

    def __init__(self, spec, goldenRunDriver):
        """Constructor.

        Arguments:
            spec: A string with the breakpoint's location.
            goldenRunDriver: The GoldenRunDriver associated to this breakpoint.
        """

        super(EndBreakpoint, self).__init__(spec)
        self.goldenRunDriver = goldenRunDriver

    def stop(self):
        """Callback invoked when this breakpoint is reached.

        Returns:
            A boolean indicating whether we should stop at this breakpoint.
        """

        self.goldenRunDriver.setInFaultZone(False)

        return True  # Stop at this breakpoint.
