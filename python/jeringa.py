# Copyright (C) 2014-2015 Martin Galvan.
# This file is part of Jeringa.
#
# Jeringa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeringa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeringa.  If not, see <http://www.gnu.org/licenses/>.

import qemu
import robusto
import runManager
import injector
import rtems
import json
import validictory
import sys
import gdb

class JeringaCommand(gdb.Command):
    """A subclass of gdb.Command used to invoke Jeringa from the gdb command line."""

    def invoke(self, jsonFile, from_tty):
        """Callback invoked from gdb when we issue the 'jeringa' command."""

        # Initial gdb setup
        gdb.execute("set pagination off")
        gdb.execute("set confirm off")
        gdb.execute("set logging file /dev/null")
        gdb.execute("set logging redirect on")

        with open(jsonFile, "r") as configFile:
            config = json.load(configFile)

        connectionManagers = {"qemu": qemu.QemuConnectionManager}
        targetOSs = {"rtems": rtems.RTEMSStackManager}
        injectors = ["stack", "symbol"]
        selfCorrectionCodes = {"robusto": robusto.RobustoDetector}

        startAndEndPointsSchema = {
            "type": "array",
            "items": {
                "type": "string"
            },
            "minItems": 1
        }

        goldenRunSchema = {
            "type": "object",
            "properties": {
                "runType": {
                    "type": "string",
                    "pattern": "golden"
                },
                "injector": {
                    "required": False
                },
                "startPoints": startAndEndPointsSchema,
                "endPoints": startAndEndPointsSchema,
                "numberOfRuns": {
                    "required:": False
                }
            }
        }

        injectionRunSchema = {
            "type": "object",
            "properties": {
                "runType": {
                    "type": "string",
                    "pattern": "injection"
                },
                "injector": {
                    "type": "string",
                    "enum": injectors
                },
                "startPoints": {
                    "required": False
                },
                "endPoints": {
                    "required": False
                },
                "numberOfRuns": {
                    "type": "integer",
                    "minimum": 1
                }
            }
        }

        configSchema = {
            "type": "object",
            "properties": {
                "goldenRunFileName": {
                    "type": "string"
                },
                "port": {
                    "type": "integer",
                    "minimum": 1
                },
                "connectionManager": {
                    "type": "string",
                    "enum": connectionManagers.keys()
                },
                "targetOS": {
                    "type": "string",
                    "enum": targetOSs.keys()
                },
                "runInfo": {
                    "type": [goldenRunSchema, injectionRunSchema]
                },
                "selfCorrectionCode": {
                    "type": "string",
                    "enum": selfCorrectionCodes.keys(),
                    "required": False
                }
            }
        }

        try:
            validictory.validate(config, configSchema)
        except:
            print("Configuration error: %s" % sys.exc_info()[1])
        else:
            # Clear any existing breakpoints.
            gdb.execute("delete")

            port = config["port"]
            connectionManager = connectionManagers[config["connectionManager"]](port)
            targetOSStackManager = targetOSs[config["targetOS"]]()

            if "selfCorrectionCode" in config:
                SCCDetector = selfCorrectionCodes[config["selfCorrectionCode"]]()
            else:
                SCCDetector = None

            goldenRunFileName = config["goldenRunFileName"]
            runType = config["runInfo"]["runType"]

            if runType == "golden":
                startPoints = config["runInfo"]["startPoints"]
                endPoints = config["runInfo"]["endPoints"]
                runMgr = runManager.GoldenRunManager(connectionManager, SCCDetector)
                runMgr.startRunning(goldenRunFileName, startPoints, endPoints)
            elif runType == "injection":
                numberOfRuns = config["runInfo"]["numberOfRuns"]
                injectorType = config["runInfo"]["injector"]

                if injectorType == "stack":
                    inj = injector.StackInjector(targetOSStackManager)
                elif injectorType == "symbol":
                    inj = injector.SymbolInjector()

                runMgr = runManager.InjectionRunManager(connectionManager, SCCDetector, inj)
                runMgr.startRunning(goldenRunFileName, numberOfRuns)

            gdb.execute("set logging off")

JeringaCommand("jeringa", gdb.COMMAND_USER)
